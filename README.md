<h3>1. Install</h3>
<strong>npm install</strong>

<hr>

<h3>2. change config.envExample to config.env<h3>

<hr>

<h3>3. Start Server</h3>
<strong>npm start</strong>

<hr>

<h3>4. Start Unit Testing</h3>
<strong>npm run test</strong>

<hr>

<h3>5. Start Sonar</h3>
<strong>npm run sonar</strong>

<hr>

testing must any user in db :<br>
{<br>
_id:60d1c9034c09d02e5cea3a2e, <br>
username:"admin", <br>
password:"$2b$10$PArVAJ1cNZUMZkUd8TLA5e4k/KaY5CryAlrIHg7CQTdYSqDQE55Iy" // 'admin' in bcrypt , <br>
name:"Mohamad Irfan Manaf", <br>
__v:0 <br>
} <br>
