var Userdb = require('../models/userModel');
const bcrypt = require('bcrypt');
const { createJwtToken } = require('../utils/jwtUtil');
const { Ok, BadRequest, InternalServerErr } = require('../utils/responseUtil')

exports.login = async (req,res) => {
    if(req.body.username == "" || req.password == "" || req.body.username == null || req.body.password == null){
        BadRequest(res, "Content can not be empty!")
        return;
    }

    let username = req.body.username;
    let password = req.body.password;
    await Userdb.find({username: username})
        .then(data => {
            if(data.length == 0){
                BadRequest(res, "Login failed, either your Username isn\'t registered in our system or your password is incorrect")
                return;
            }else{
                bcrypt.compare(password, data[0].password, function(err, result){
                    if(result){
                        let userLogin = {}
                        userLogin.token = createJwtToken(username)
                        userLogin.data = data[0]
                        Ok(res, "Login success", userLogin)
                    }else{
                        BadRequest(res, "Login failed, password incorrect")
                        return;
                    }
                })
            }
        })
}

exports.logout = async (req,res) => {
    Ok(res, "Logout Success")
}