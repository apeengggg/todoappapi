const { ok } = require('assert');
var todoDB = require('../models/todoModel');
const { Ok, BadRequest, InternalServerErr } = require('../utils/responseUtil')

exports.create = async (req,res) => {
    try{
        let todoDesc = req.body.todoDesc;
        let targetDate = req.body.targetDate;
        let isDone = req.body.isDone;
        if(targetDate != ''){
            let today = new Date().toISOString().substr(0,10)
            targetDate = targetDate.split("-")
            targetDate = targetDate[2]+"-"+targetDate[1]+"-"+targetDate[0]
            if(targetDate < today){
                BadRequest(res, "Target Date Must Greater Than Today!")
                return
            }
        }
            const todo = new todoDB({
                todoDesc: todoDesc,
                targetDate: targetDate,
                isDone: isDone,
                username : req.app.locals.username
            })

            const saving = await todo.save(todo)
            if(saving){
                Ok(res, "Todo Created Successfully", saving)
            }
    }catch(err){
        InternalServerErr(res,  err.message)
    }
}

exports.find = async (req, res)=>{
    const username = req.app.locals.username;
    try{
        const find = await todoDB.find({username: username})
        if(find){
            Ok(res, "Todo's Found", find)
        }
    }catch(err){
        InternalServerErr(res, err.message)
    }
}

exports.update = async (req, res)=>{
    let username
    let targetDate
    try{
        const finding = await todoDB.findById(req.params.id)
        if(finding){
            username = finding.username
        }
        if(req.app.locals.username == username){
            if(req.body.targetDate != ''){
                let today = new Date().toISOString().substr(0,10)
                let todoDate = req.body.targetDate.split("-")
                targetDate = todoDate[2]+"-"+todoDate[1]+"-"+todoDate[0]
                if(targetDate < today){
                    BadRequest(res, "Target Date Must Greater Than Today!")
                    return
                }
            }
                const updating = await todoDB.findByIdAndUpdate(req.params.id, req.body, { useFindAndModify: false})
                let item = {}
                item.oldData = updating
                item.newData = req.body
                Ok(res, "Todo Updated", item)
        }else{
            BadRequest(res, "You Can't Edit This Todo, You Not Owner This Todo")
        }
    }catch(err){
        InternalServerErr(res, err.message)
        return
    }
}

exports.delete = async (req, res)=>{
    const id = req.params.id;
    let username
    try{
        const find = await todoDB.findById(id)
        if(find){
            username = find.username
            if(username == req.app.locals.username){
                await todoDB.findByIdAndDelete(id)
                Ok(res, 'Todo was deleted successfully!')
            }else{
                BadRequest(res, "This Todo Is Not Belongs To You")
            }
        }
    }catch(err){
        InternalServerErr(res, err.message)
    }
}