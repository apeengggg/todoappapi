var Userdb = require('../models/userModel');
const bcrypt = require('bcrypt')
const saltRounds = parseInt(process.env.SALT_ROUND)
const { Ok, BadRequest, InternalServerErr } = require('../utils/responseUtil');
const todoDB = require('../models/todoModel');

exports.create = async (req,res) => {
    let username = req.body.username;
    let name = req.body.name;
    try{
        let password = await bcrypt.hash(req.body.password, saltRounds);
        const find = await Userdb.find({username: username})
        if(find.length > 0){
            BadRequest(res, "Username is exist!")
            return
        }

        const user = new Userdb({
            username: username,
            password: password,
            name: name
        })
        const saving = await user.save(user)
        if(saving){
            Ok(res, "User Created Successfully", saving)
        }
    }catch(err){
        InternalServerErr(res, err.message)
    }
}

exports.find = async (req, res)=>{
    try{
        const username = req.app.locals.username;
        const user = await Userdb.find({username: username})
            if(user) {
                Ok(res, "User Found"+"[ "+user.length+" ]", user)
            }
    }catch(err){
        InternalServerErr(res, err.message)
    }
}

exports.update = async (req, res)=> {
    const id = req.params.id;
    let username
    let owner = req.app.locals.username
    try{
        const user = await Userdb.findById(id)
        if(user){
            username = user.username
            if(username != owner){
                BadRequest(res, "You cant update other user data")
                return
            }
            if(req.body.username != username){
                BadRequest(res, "You Cant Edit Your Username")
                return
            }
            req.body.password = await bcrypt.hash(req.body.password, saltRounds)
            const updating = await Userdb.findByIdAndUpdate(id, req.body, { useFindAndModify: false})
                let item = {}
                item.oldData = updating
                item.newData = req.body
                Ok(res, "User Updated Successfully", item)
        }
    }catch(err){
        InternalServerErr(res, err.message)
        return
    }
}

exports.delete = async (req, res)=>{
    const id = req.params.id;
    let owner = req.app.locals.username
    let username
        try{
            const find = await Userdb.findById(id)
            if(find){
                username = find.username
                if(owner != username){
                    BadRequest(res, "You Can't Delete Other User Data")
                    return
                }
                await todoDB.deleteMany({username: username})
                await Userdb.findByIdAndDelete(id)
                Ok(res, "Delete user successfully")
            }
        }catch(err){
            InternalServerErr(res, err.message)
            return
        }
}