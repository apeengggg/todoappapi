const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const morgan = require('morgan')
const dotenv = require('dotenv')
const routes = require('./routes')
const PORT = process.env.PORT || 3000

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.text());
app.use(bodyParser.json({ type: 'application/json'}));

dotenv.config( { path : 'config.env'} )

const connectDB = require('./database/connection');
connectDB();

// app.get('/', (req, res) => {
//     res.send('This Work')
// });

app.use('/apiTodo/v1/', routes)

app.listen(PORT, () => {
    console.log(`Server started on http://localhost:${PORT}`);
});
