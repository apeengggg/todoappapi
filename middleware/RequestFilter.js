const { verifyJwt } = require('../utils/jwtUtil')
const { BadRequest, Unauthorized } = require('../utils/responseUtil')

var Userdb = require('../models/userModel');
const getToken = (bearer) => {
    return bearer.slice(7, bearer.length)
}

const JwtFilter = async (req, res, next) => {
    let token
    if(req.headers.authorization) {
        token = getToken(req.headers.authorization)
    }

    if(token) {
        if(verifyJwt(req, token)) {
            const { username } = req.app.locals
            await Userdb.find({username: username})
                    .then(data => {
                        if(data.length == 0){
                            BadRequest(res, "User does not exist anymore")
                        }else{
                            next();
                        }
                    })
        } else {
            Unauthorized(res, "Token is not valid")
        }
    } else {
        Unauthorized(res, "Token is not valid")
    }
}

module.exports = { JwtFilter }
