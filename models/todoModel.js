const mongoose = require('mongoose');

var schema = new mongoose.Schema({
    todoDesc : {
        type : String,
        required: true,
    },
    targetDate : {
        type: Date,
        required: false,
    },
    isDone : {
        type: Boolean,
        required: true
    },
    username : {
        type: String,
        require: true
    }
})

const TodoDB = mongoose.model('tododb', schema);

module.exports = TodoDB;