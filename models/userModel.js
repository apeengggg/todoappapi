const mongoose = require('mongoose');

var schema = new mongoose.Schema({
    username : {
        type : String,
        required: true,
        unique: true,
        min: 3,
        max: 10,
        lowercase: true
    },
    password : {
        type: String,
        required: true,
        min: 3,
        max: 1024
    },
    name : {
        type: String,
        required: true
    }
})

const Userdb = mongoose.model('userdb', schema);

module.exports = Userdb;