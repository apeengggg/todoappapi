const express = require('express');
const router = express.Router()

const ctrl = require('../controllers/authController');

const ReqFilter = require('../middleware/RequestFilter')


router.post('/login', ctrl.login)
router.post('/logout', ReqFilter.JwtFilter, ctrl.logout)

module.exports = router