const express = require('express');
const router = express.Router()

const ctrl = require('../controllers/userController');

const ReqFilter = require('../middleware/RequestFilter')

router.route('/')
    .get(ReqFilter.JwtFilter, ctrl.find)
    .post(ctrl.create)
router.route('/:id')
    .put(ReqFilter.JwtFilter, ctrl.update)
    .delete(ReqFilter.JwtFilter, ctrl.delete)

module.exports = router