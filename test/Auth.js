const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../index');
const {response} = require('express')
const expect = chai.expect

chai.should();
chai.use(chaiHttp);

let token = '';

describe('Test Login User', () => {
    it('Login with username = "" and password = ""', (done) => {
        chai.request('http://localhost:3000').post('/apiTodo/v1/auth/login')
        .send({
            'username' : '',
            'password' : ''
        }).end((err, res) => {
            expect(res.body).to.have.property('statusCode')
            expect(res.body).to.have.property('message')
            expect(res.body).to.have.property('error')
            expect(res).to.have.be.status(400)
            done();
        })
    })

    it('Login with wrong username (not in DB ) and right password', (done) => {
        chai.request('http://localhost:3000').post('/apiTodo/v1/auth/login')
        .send({
            'username' : 'wrongUsername',
            'password' : 'admin'
        }).end((err, res) => {
            expect(res.body).to.have.property('statusCode')
            expect(res.body).to.have.property('message')
            expect(res.body).to.have.property('error')
            expect(res).to.have.be.status(400)
            done();
        })
    })

    it('Login with right username (in db) and wrong password', (done) => {
        chai.request('http://localhost:3000').post('/apiTodo/v1/auth/login')
        .send({
            'username' : 'admin',
            'password' : 'admin123123123'
        }).end((err, res) => {
            expect(res.body).to.have.property('statusCode')
            expect(res.body).to.have.property('message')
            expect(res.body).to.have.property('error')
            expect(res).to.have.be.status(400)
            done();
        })
    })

    it('Success Login', (done) => {
        chai.request('http://localhost:3000').post('/apiTodo/v1/auth/login')
        .send({
            'username' : 'admin',
            'password' : 'admin'
        }).end((err, res) => {
            expect(res.body.data).to.have.property('token');
            token = res.body.data.token;
            expect(res).to.have.be.status(200)
            done();
        })
    })

    it('Success Logout', (done) => {
        chai.request('http://localhost:3000').post('/apiTodo/v1/auth/logout').set('Authorization',`Bearer ${token}`)
        .end((err, res) => {
            expect(res).to.have.be.status(200)
            expect(res.body).to.have.property('statusCode')
            expect(res.body).to.have.property('message')
            done();
        })
    })
})