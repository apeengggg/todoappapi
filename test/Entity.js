const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../index');
const {response} = require('express')
const expect = chai.expect
const { createJwtToken } = require('../utils/jwtUtil')
chai.should();
chai.use(chaiHttp);

let usernameFake = 'dadang';
let otherToken = createJwtToken(usernameFake);
let token = '';
let fakeToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjoiYWRtaW4iLCJpYXQiOjE2MjQ0MzQzMzYsImV4cCI6MTYyNDQ3NzUzNn0.8mf3LB6pykn8UnJfaMRPR_FmKZtjewxKEMQnkg-x6FMS';

let userId = ''
let FakeUserId = userId+"0"
let otherUserId = '60d1c9034c09d02e5cea3a2e'

let todoId = ''
let fakeTodoId = todoId+"0"
let otherTodoId = '60d1d53c478612e12ecc2ca1'

describe('Test User Entity', () => {
    it('Register with all or one of content = ""', (done) => {
        chai.request('http://localhost:3000').post('/apiTodo/v1/users')
        .send({
            'username' : '',
            'password' : 'admin',
            'name' : 'Mohamad Irfan Manaf'
        }).end((err, res) => {
            expect(res.body).to.have.property('statusCode')
            expect(res.body).to.have.property('message')
            expect(res.body).to.have.property('error')
            expect(res).to.have.be.status(500)
            done();
        })
    })

    it('Register with username is exist in db', (done) => {
        chai.request('http://localhost:3000').post('/apiTodo/v1/users')
        .send({
            'username' : 'admin',
            'password' : 'admin',
            'name' : 'Mohamad Irfan Manaf'
        }).end((err, res) => {
            expect(res.body).to.have.property('statusCode')
            expect(res.body).to.have.property('message')
            expect(res.body).to.have.property('error')
            expect(res).to.have.be.status(400)
            done();
        })
    })

    it('Register with username is not exist in db', (done) => {
        chai.request('http://localhost:3000').post('/apiTodo/v1/users')
        .send({
            'username' : 'manaf',
            'password' : 'admin',
            'name' : 'Mohamad Irfan Manaf'
        }).end((err, res) => {
            expect(res.body).to.have.property('statusCode')
            expect(res.body).to.have.property('message')
            expect(res.body).to.have.property('data')
            userId = res.body.data._id
            expect(res).to.have.be.status(200)
            done();
        })
    })

    it('Login with new registered user', (done) => {
        chai.request('http://localhost:3000').post('/apiTodo/v1/auth/login')
        .send({
            'username' : 'manaf',
            'password' : 'admin'
        }).end((err, res) => {
            expect(res.body.data).to.have.property('token');
            token = res.body.data.token;
            expect(res).to.have.be.status(200)
            done();
        })
    })

    it('Find Data User (with token filter)', (done) => {
        chai.request('http://localhost:3000').get('/apiTodo/v1/users').set('Authorization',`Bearer ${token}`)
        .end((err, res) => {
            expect(res).to.have.be.status(200)
            expect(res.body).to.have.property('statusCode')
            expect(res.body).to.have.property('message')
            expect(res.body).to.have.property('data')
            done();
        })
    })

    it('Find Data User (with WRONG FAKE token filter)', (done) => {
        chai.request('http://localhost:3000').get('/apiTodo/v1/users').set('Authorization',`Bearer ${fakeToken}`)
        .end((err, res) => {
            expect(res).to.have.be.status(401)
            expect(res.body).to.have.property('statusCode')
            expect(res.body).to.have.property('message')
            expect(res.body).to.have.property('error')
            done();
        })
    })

    it('Find Data User (with WRONG OTHER token filter)', (done) => {
        chai.request('http://localhost:3000').get('/apiTodo/v1/users').set('Authorization',`Bearer ${otherToken}`)
        .end((err, res) => {
            expect(res).to.have.be.status(400)
            expect(res.body).to.have.property('statusCode')
            expect(res.body).to.have.property('message')
            expect(res.body).to.have.property('error')
            done();
        })
    })

    it('Update User but id is not in DB', (done) => {
        chai.request('http://localhost:3000').put(`/apiTodo/v1/users/${FakeUserId}`).set('Authorization',`Bearer ${token}`)
        .send({
            username : "manaf",
            password : "admin",
            name : "Mohamad Irfan Manaf"
        })
        .end((err, res) => {
            expect(res).to.have.be.status(500)
            expect(res.body).to.have.property('statusCode')
            expect(res.body).to.have.property('message')
            expect(res.body).to.have.property('error')
            done();
        })
    })

    it('Update User but one or all content = ""', (done) => {
        chai.request('http://localhost:3000').put(`/apiTodo/v1/users/${userId}`).set('Authorization',`Bearer ${token}`)
        .send({
            username : "",
            password : "",
            name : "Mohamad Irfan Manaf"
        })
        .end((err, res) => {
            expect(res).to.have.be.status(400)
            expect(res.body).to.have.property('statusCode')
            expect(res.body).to.have.property('message')
            expect(res.body).to.have.property('error')
            done();
        })
    })

    it('Update User data but data user is not have (logged in) owner', (done) => {
        chai.request('http://localhost:3000').put(`/apiTodo/v1/users/${otherUserId}`).set('Authorization',`Bearer ${token}`)
        .send({
            username : "admin",
            password : "admin123",
            name : "Mohamad Irfan Manaf"
        })
        .end((err, res) => {
            expect(res).to.have.be.status(400)
            expect(res.body).to.have.property('statusCode')
            expect(res.body).to.have.property('message')
            expect(res.body).to.have.property('error')
            done();
        })
    })

    it('Update User the data but username is changed', (done) => {
        chai.request('http://localhost:3000').put(`/apiTodo/v1/users/${userId}`).set('Authorization',`Bearer ${token}`)
        .send({
            username : "admin", // the old data = 'manaf'
            password : "admin",
            name : "Mohamad Irfan Manaf"
        })
        .end((err, res) => {
            expect(res).to.have.be.status(400)
            expect(res.body).to.have.property('statusCode')
            expect(res.body).to.have.property('message')
            expect(res.body).to.have.property('error')
            done();
        })
    })

    it('Update User Data (Success)', (done) => {
        chai.request('http://localhost:3000').put(`/apiTodo/v1/users/${userId}`).set('Authorization',`Bearer ${token}`)
        .send({
            username : "manaf", // the old data = 'manaf'
            password : "admin123",
            name : "Mohamad Irfan Manaf Edited"
        })
        .end((err, res) => {
            expect(res).to.have.be.status(200)
            expect(res.body).to.have.property('statusCode')
            expect(res.body).to.have.property('message')
            expect(res.body).to.have.property('data')
            done();
        })
    })

    it('Delete User With Wrong Id', (done) => {
        chai.request('http://localhost:3000').delete(`/apiTodo/v1/users/${FakeUserId}`).set('Authorization',`Bearer ${token}`)
        .end((err, res) => {
            expect(res).to.have.be.status(500)
            expect(res.body).to.have.property('statusCode')
            expect(res.body).to.have.property('message')
            expect(res.body).to.have.property('error')
            done();
        })
    })

    it('Delete User Data Other User (not user logged in)', (done) => {
        chai.request('http://localhost:3000').delete(`/apiTodo/v1/users/${otherUserId}`).set('Authorization',`Bearer ${token}`)
        .end((err, res) => {
            expect(res).to.have.be.status(400)
            expect(res.body).to.have.property('statusCode')
            expect(res.body).to.have.property('message')
            expect(res.body).to.have.property('error')
            done();
        })
     })
})

    describe('Test Todo Entity', () => {
        it('Saving Todo With Todo Desc / Is Done = ""', (done) => {
            chai.request('http://localhost:3000').post('/apiTodo/v1/todo').set('Authorization',`Bearer ${token}`)
            .send({
                'todoDesc' : '',
                'targetDate' : '30-06-2021',
                'isDone' : 0
            }).end((err, res) => {
                expect(res.body).to.have.property('statusCode')
                expect(res.body).to.have.property('message')
                expect(res.body).to.have.property('error')
                expect(res).to.have.be.status(500)
                done();
            })
        })

        it('Saving Todo With targetDate < today Date ', (done) => {
            chai.request('http://localhost:3000').post('/apiTodo/v1/todo').set('Authorization',`Bearer ${token}`)
            .send({
                'todoDesc' : 'Todo Desc2',
                'targetDate' : '01-06-2021',
                'isDone' : 0
            }).end((err, res) => {
                expect(res.body).to.have.property('statusCode')
                expect(res.body).to.have.property('message')
                expect(res.body).to.have.property('error')
                expect(res).to.have.be.status(400)
                done();
            })
        })

        it('Saving Todo With targetDate = ""', (done) => {
            chai.request('http://localhost:3000').post('/apiTodo/v1/todo').set('Authorization',`Bearer ${token}`)
            .send({
                'todoDesc' : 'Todo Desc2',
                'targetDate' : '',
                'isDone' : 0
            }).end((err, res) => {
                expect(res.body).to.have.property('statusCode')
                expect(res.body).to.have.property('message')
                expect(res.body).to.have.property('data')
                expect(res).to.have.be.status(200)
                done();
            })
        })

        it('Saving Todo With targetDate not empty and targetDate >= today Date', (done) => {
            chai.request('http://localhost:3000').post('/apiTodo/v1/todo').set('Authorization',`Bearer ${token}`)
            .send({
                'todoDesc' : 'Todo Desc2',
                'targetDate' : '30-06-2021',
                'isDone' : 0
            }).end((err, res) => {
                expect(res.body).to.have.property('statusCode')
                expect(res.body).to.have.property('message')
                expect(res.body).to.have.property('data')
                todoId = res.body.data._id
                expect(res).to.have.be.status(200)
                done();
            })
        })

        it('Find Todo With Real Token', (done) => {
            chai.request('http://localhost:3000').get('/apiTodo/v1/todo').set('Authorization',`Bearer ${token}`)
            .end((err, res) => {
                expect(res.body).to.have.property('statusCode')
                expect(res.body).to.have.property('message')
                expect(res.body).to.have.property('data')
                expect(res).to.have.be.status(200)
                done();
            })
        })

        it('Find Todo With Fake Token', (done) => {
            chai.request('http://localhost:3000').get('/apiTodo/v1/todo').set('Authorization',`Bearer ${fakeToken}`)
            .end((err, res) => {
                expect(res.body).to.have.property('statusCode')
                expect(res.body).to.have.property('message')
                expect(res.body).to.have.property('error')
                expect(res).to.have.be.status(401)
                done();
            })
        })

        it('Find Todo With Other Token', (done) => {
            chai.request('http://localhost:3000').get('/apiTodo/v1/todo').set('Authorization',`Bearer ${otherToken}`)
            .end((err, res) => {
                expect(res.body).to.have.property('statusCode')
                expect(res.body).to.have.property('message')
                expect(res.body).to.have.property('error')
                expect(res).to.have.be.status(400)
                done();
            })
        })

        it('Updating Todo (Search Todo with Wrong Id)', (done) => {
            chai.request('http://localhost:3000').put(`/apiTodo/v1/todo/${fakeTodoId}`).set('Authorization',`Bearer ${token}`)
            .end((err, res) => {
                expect(res.body).to.have.property('statusCode')
                expect(res.body).to.have.property('message')
                expect(res.body).to.have.property('error')
                expect(res).to.have.be.status(500)
                done();
            })
        })

        it('Updating Todo (Search Todo with Right Id) but data Todo is not have (logged in) owner', (done) => {
            chai.request('http://localhost:3000').put(`/apiTodo/v1/todo/${otherTodoId}`).set('Authorization',`Bearer ${token}`)
            .send({
                todoDesc : 'Todo Desc Edited',
                targetDate : '01-06-2021',
                isDone : 1
            })
            .end((err, res) => {
                expect(res.body).to.have.property('statusCode')
                expect(res.body).to.have.property('message')
                expect(res.body).to.have.property('error')
                expect(res).to.have.be.status(400)
                done();
            })
        })

        it('Updating Todo (Search Todo with Right Id) with targetDate < todayDate', (done) => {
            chai.request('http://localhost:3000').put(`/apiTodo/v1/todo/${todoId}`).set('Authorization',`Bearer ${token}`)
            .send({
                todoDesc : 'Todo Desc Edited',
                targetDate : '01-06-2021',
                isDone : 1
            })
            .end((err, res) => {
                expect(res.body).to.have.property('statusCode')
                expect(res.body).to.have.property('message')
                expect(res.body).to.have.property('error')
                expect(res).to.have.be.status(400)
                done();
            })
        })

        it('Updating Todo with no token', (done) => {
            chai.request('http://localhost:3000').put(`/apiTodo/v1/todo/${todoId}`)
            .send({
                todoDesc : 'Todo Desc Edited',
                targetDate : '01-06-2021',
                isDone : 1
            })
            .end((err, res) => {
                expect(res.body).to.have.property('statusCode')
                expect(res.body).to.have.property('message')
                expect(res.body).to.have.property('error')
                expect(res).to.have.be.status(401)
                done();
            })
        })

        it('Updating Todo (Search Todo with Right Id) with targetDate = ""', (done) => {
            chai.request('http://localhost:3000').put(`/apiTodo/v1/todo/${todoId}`).set('Authorization',`Bearer ${token}`)
            .send({
                todoDesc : 'Todo Desc Edited',
                targetDate : '',
                isDone : 1
            })
            .end((err, res) => {
                expect(res.body).to.have.property('statusCode')
                expect(res.body).to.have.property('message')
                expect(res.body).to.have.property('data')
                expect(res).to.have.be.status(200)
                done();
            })
        })

        it('Updating Todo (Search Todo with Right Id) Right Data', (done) => {
            chai.request('http://localhost:3000').put(`/apiTodo/v1/todo/${todoId}`).set('Authorization',`Bearer ${token}`)
            .send({
                todoDesc : 'Todo Desc Edited Right Data',
                targetDate : '01-07-2021',
                isDone : 1
            })
            .end((err, res) => {
                expect(res.body).to.have.property('statusCode')
                expect(res.body).to.have.property('message')
                expect(res.body).to.have.property('data')
                expect(res).to.have.be.status(200)
                done();
            })
        })

        it('Delete Todo With Fake Id', (done) => {
            chai.request('http://localhost:3000').delete(`/apiTodo/v1/todo/${fakeTodoId}`).set('Authorization',`Bearer ${token}`)
            .end((err, res) => {
                expect(res.body).to.have.property('statusCode')
                expect(res.body).to.have.property('message')
                expect(res.body).to.have.property('error')
                expect(res).to.have.be.status(500)
                done();
            })
        })

        it('Delete Todo but Todo data is not have (logged in) owner', (done) => {
            chai.request('http://localhost:3000').delete(`/apiTodo/v1/todo/${otherTodoId}`).set('Authorization',`Bearer ${token}`)
            .end((err, res) => {
                expect(res.body).to.have.property('statusCode')
                expect(res.body).to.have.property('message')
                expect(res.body).to.have.property('error')
                expect(res).to.have.be.status(400)
                done();
            })
        })

        it('Delete Todo with Right ID', (done) => {
            chai.request('http://localhost:3000').delete(`/apiTodo/v1/todo/${todoId}`).set('Authorization',`Bearer ${token}`)
            .end((err, res) => {
                expect(res.body).to.have.property('statusCode')
                expect(res.body).to.have.property('message')
                expect(res).to.have.be.status(200)
                done();
            })
        })

    })

    describe('Test User Entity Deleting User', () => {
        it('Delete User (Success)', (done) => {
        chai.request('http://localhost:3000').delete(`/apiTodo/v1/users/${userId}`).set('Authorization',`Bearer ${token}`)
        .end((err, res) => {
            expect(res).to.have.be.status(200)
            expect(res.body).to.have.property('statusCode')
            expect(res.body).to.have.property('message')
            done();
        })
    })
})