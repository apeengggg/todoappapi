const jwt = require('jsonwebtoken')

const verifyJwt = (req, token) => {
    try {
        var decoded = jwt.verify(token, process.env.JWT_SECRET);
        req.app.locals.username = decoded.data
        return true
    } catch(err) {
        return false
    }
}

const createJwtToken = (username) => {
    let expires = process.env.JWT_EXPIRED
    return jwt.sign({
        data: username,
    }, process.env.JWT_SECRET, { expiresIn: expires });
}

module.exports = { verifyJwt, createJwtToken }